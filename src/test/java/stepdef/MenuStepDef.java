package stepdef;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class MenuStepDef {
    WebDriver driver;

    @Given("^I navigate to the home page$")
    public void i_navigate_to_the_home_page() throws Throwable {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("https://ag-space.com/");
        driver.manage().window().maximize();
    }

    @When("^I click on the menu item (.*)$")
    public void iClickOnTheMenuItemMenuitem(String menuItem) throws Throwable {
        try{

            driver.findElement(By.linkText(menuItem.toUpperCase())).click();

        }catch (Exception e){
            driver.quit();
        }

    }


    @Then("^check the page lands on the correct url (.*)$")
    public void checkThePageLandsOnTheCorrectUrlLandingurl(String landingUrl) throws Throwable {

        try{
            Assert.assertEquals(landingUrl, driver.getCurrentUrl());
        }
        catch(Throwable pageNavigationError){
            System.out.println("Didn't navigate to correct url");
        }
        finally {
            driver.quit();
        }

    }


}
