package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/ag-space"},
        glue = {"stepdef"},
        format= {"pretty","html:test-outout"},
        tags = {"@SmokeTests,@RegressionTest"}
)

public class MyRunner {

}
