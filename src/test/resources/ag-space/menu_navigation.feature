Feature: Menu

@SmokeTests
Scenario Outline: Menu Items when clicked lands on correct url

  Given I navigate to the home page
  When I click on the menu item <menuitem>
  Then check the page lands on the correct url <landingurl>
  Examples:
    | menuitem          | landingurl                              |
    | Innovation        | https://ag-space.com/innovation/        |
    | Big Data          | https://ag-space.com/bigdata/           |
    | Earth observation | https://ag-space.com/earth-observation/ |
    | Contour           | https://ag-space.com/contour/           |
    | Grid              | https://ag-space.com/grid/              |
    | Contact           | https://ag-space.com/contact/           |
    | What’s new        | https://ag-space.com/whats-new/         |