$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("menu_navigation.feature");
formatter.feature({
  "line": 1,
  "name": "Menu",
  "description": "",
  "id": "menu",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Menu Items when clicked lands on correct url",
  "description": "",
  "id": "menu;menu-items-when-clicked-lands-on-correct-url",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@SmokeTests"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I navigate to the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I click on the menu item \u003cmenuitem\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "check the page lands on the correct url \u003clandingurl\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "menu;menu-items-when-clicked-lands-on-correct-url;",
  "rows": [
    {
      "cells": [
        "menuitem",
        "landingurl"
      ],
      "line": 10,
      "id": "menu;menu-items-when-clicked-lands-on-correct-url;;1"
    },
    {
      "cells": [
        "Innovation",
        "https://ag-space.com/innovation/"
      ],
      "line": 11,
      "id": "menu;menu-items-when-clicked-lands-on-correct-url;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Menu Items when clicked lands on correct url",
  "description": "",
  "id": "menu;menu-items-when-clicked-lands-on-correct-url;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@SmokeTests"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I navigate to the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I click on the menu item Innovation",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "check the page lands on the correct url https://ag-space.com/innovation/",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "MenuStepDef.i_navigate_to_the_home_page()"
});
formatter.result({
  "duration": 6057632382,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Innovation",
      "offset": 25
    }
  ],
  "location": "MenuStepDef.iClickOnTheMenuItemMenuitem(String)"
});
formatter.result({
  "duration": 1338630929,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://ag-space.com/innovation/",
      "offset": 40
    }
  ],
  "location": "MenuStepDef.checkThePageLandsOnTheCorrectUrlLandingurl(String)"
});
formatter.result({
  "duration": 453060560,
  "status": "passed"
});
});